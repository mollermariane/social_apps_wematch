﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using WeMatch.Business;
using WeMatch.Repository;
using WeMatch.WebModels;
using WeMatch.WebModels.Enum;

namespace WeMatch.Web.Controllers
{
    public class HomeController : Controller
    {
        public CaronaBO CaronaBO { get; set; }

        public HomeController()
        {
            IFactoryContexto dbFactory = new FactoryContexto(System.Web.HttpContext.Current);
            IContextoRepo contexto = dbFactory.ObterContextoAtual<ContextoRepo>();

            CaronaBO = new CaronaBO(contexto);
        }

        public async Task<ActionResult> Index()
        {
            DisplayModel model = new DisplayModel();
            var listaCaronaSatisfeita = CaronaBO.ObterMatchs();
            var listaOferecimento = CaronaBO.ObterTodosOsOferecimentos();
            var listaSolicitacoes = CaronaBO.ObterTodasAsSolicitacoes();

            if (listaOferecimento != null && listaOferecimento.Any())
            {
                model.ListaOferecimentos = (from l in listaOferecimento
                                            select new DisplayItemModel
                                            {
                                                IsOferecimentoCarona = true,
                                                Data = l.DataSolicitacao,
                                                MensagemPost = string.Format("{0} oferece carona<br/>para {1}.<br/>Cel.: {2}<br/>{3}", ToTitleCase(l.Nome),
                                                GetNomeCidade(l.IdCidadeDestino),
                                                FormataString("(##)#####-####", l.Telefone.Replace("55", "")),
                                                l.DataSolicitacao.ToString("HH:mm"))
                                            }).ToList();
            }

            if (listaCaronaSatisfeita != null && listaCaronaSatisfeita.Any())
            {
                model.ListaCaronaSatisfeita = (from l in listaCaronaSatisfeita
                                               select new DisplayItemModel
                                               {
                                                   MensagemPost = string.Format("Ocorreu um Match<br/>entre {0}<br/>e {1}.<br/>Cidade: {2}", ToTitleCase(l.OferecoCarona.Nome),
                                                   ToTitleCase(l.QueroCarona.Nome),
                                                   GetNomeCidade(l.OferecoCarona.IdCidadeDestino))
                                               }).ToList();
            }

            if (listaSolicitacoes != null && listaSolicitacoes.Any())
            {
                if(model.ListaOferecimentos == null || !model.ListaOferecimentos.Any())
                    model.ListaOferecimentos = new List<DisplayItemModel>();

                model.ListaOferecimentos.AddRange((from l in listaSolicitacoes
                                                   select new DisplayItemModel
                                                   {
                                                       IsOferecimentoCarona = false,
                                                       Data = l.DataSolicitacao,
                                                       MensagemPost = string.Format("{0} solicitou<br/>carona para {1}.<br/>Cel.: {2}<br/>{3}", ToTitleCase(l.Nome),
                                                        GetNomeCidade(l.IdCidadeDestino),
                                                        FormataString("(##)#####-####", l.Telefone.Replace("55", "")),
                                                        l.DataSolicitacao.ToString("HH:mm"))
                                                   }).ToList());
            }

            if (model != null && model.ListaOferecimentos != null && model.ListaOferecimentos.Any())
            {
                model.ListaOferecimentos = model.ListaOferecimentos.OrderByDescending(l => l.Data).ToList();

                StringBuilder divsCategorias = new StringBuilder();

                for (int i = 0; i < model.ListaOferecimentos.Count; i++)
                {
                    if (model.ListaOferecimentos[i].IsOferecimentoCarona)
                        divsCategorias.Append("<div class=\"categoria\"><div id=\"" + i + "\" class=\"imgcategoria\" style=\"box-shadow: none!important; background-image: url('http://4.bp.blogspot.com/-RCQEpMf7rGI/Tc8Axvf7QDI/AAAAAAAAARU/9gQX0uXQBgM/s1600/Post-it_Green.png'); height:265px;\"><h3 style=\"padding-top:85px; margin-left: 5px;  font-size: 19px; color: #000; text-shadow: none!important; text-align: center; width: 206px;\">" + model.ListaOferecimentos[i].MensagemPost + "</h3><div class=\"thumbcontainer\"></div></div></div>");
                    else
                        divsCategorias.Append("<div class=\"categoria\"><div id=\"" + i + "\" class=\"imgcategoria\" style=\"box-shadow: none!important; background-image: url('http://1.bp.blogspot.com/-ywKlrpnUnqo/Tc8Axuwu_mI/AAAAAAAAARY/V1EBr_BYHMs/s1600/Post-it_Yellow.png'); height:265px;\"><h3 style=\"padding-top:80px; margin-left: 5px;  font-size: 19px; color: #000; text-shadow: none!important; text-align: center; width: 209px;\">" + model.ListaOferecimentos[i].MensagemPost + "</h3><div class=\"thumbcontainer\"></div></div></div>");
                }

                ViewBag.divsCategorias = divsCategorias;
            }

            StringBuilder divsImagens = new StringBuilder();
            divsImagens.Append("<div class=\"item quero-carona\" style=\"background-image: url('http://grabbaggraphics.com/wp-content/uploads/2014/05/Corkboard-Texture-02.jpg');\"><div class=\"container\"><div class=\"carousel-caption\"><img src=\"http://blog.cellep.com/wp-content/uploads/2011/09/carona.png\" style=\"width:200px;\"></img><b><h1 style=\"font-size: 27px;\">Procura por carona perto de você?<br />Mande mensagem via What's App para o número (19)99944-4405<br/>1º) Procuro + NOME_CIDADE <br /> 2º) Share your location</h1></b></div></div></div>");
            divsImagens.Append("<div class=\"item ofereco-carona\" style=\"background-image: url('http://grabbaggraphics.com/wp-content/uploads/2014/05/Corkboard-Texture-02.jpg');\"><div class=\"container\"><div class=\"carousel-caption\"><img src=\"http://www.cidademineira.com.br/imgs/doucarona.png\" style=\"width:400px;\"></img><h1 style=\"font-size: 27px;\">Quer oferecer carona perto de você?<br />Mande mensagem via What's App para o número (19)99944-4405<br/>1º) Ofereço + NOME_CIDADE <br /> 2º) Share your location</h1></div></div></div>");
            ViewBag.divsImagens = divsImagens;

            return View();
        }

        protected string FormataString(string mascara, string valor)
        {
            string novoValor = string.Empty;
            int posicao = 0;
            for (int i = 0; mascara.Length > i; i++)
            {
                if (mascara[i] == '#')
                {
                    if (valor.Length > posicao)
                    {
                        novoValor = novoValor + valor[posicao];
                        posicao++;
                    }
                    else
                        break;
                }
                else
                {
                    if (valor.Length > posicao)
                        novoValor = novoValor + mascara[i];
                    else
                        break;
                }
            }
            return novoValor;
        }

        public string ToTitleCase(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            return textInfo.ToTitleCase(s.ToLower());
        }

        private string GetNomeCidade(int p)
        {
            switch (p)
            {
                case (int)EnumCidade.Americana:
                    return "Americana";
                case (int)EnumCidade.Campinas:
                    return "Campinas";
                case (int)EnumCidade.Indaiatuba:
                    return "Indaiatuba";
                case (int)EnumCidade.MonteMor:
                    return "Monte Mor";
                case (int)EnumCidade.NovaOdesa:
                    return "Nova Odessa";
                case (int)EnumCidade.Sumare:
                    return "Sumaré";
                default:
                    return null;
            }
        }
    }
}
