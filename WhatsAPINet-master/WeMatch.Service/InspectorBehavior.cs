﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    public class InspectorBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint,
           System.ServiceModel.Channels.BindingParameterCollection
                                                bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint,
                System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            var clientInspector = new ClientInspector();
            clientRuntime.MessageInspectors.Add(clientInspector);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint,
             System.ServiceModel.Dispatcher.EndpointDispatcher
                                              endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }
}
