﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    public partial class Service1 : ServiceBase
    {
        ITaskScheduler scheduler;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            scheduler = new TaskScheduler();
            scheduler.Run();
        }

        protected override void OnStop()
        {
            if (scheduler != null)
                scheduler.Stop();
        }
    }
}
