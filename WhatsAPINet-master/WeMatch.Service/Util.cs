﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    internal static class Util
    {
        private static string serviceName = null;

        /// <summary>
        /// Extracts the Service Name from config file.
        /// </summary>
        public static string ServiceName
        {
            get
            {
                if (serviceName == null)
                {
                    Configuration config =
                    System.Configuration.ConfigurationManager.OpenExeConfiguration(
                    Assembly.GetExecutingAssembly().Location);

                    serviceName =
                    config.AppSettings.Settings["ServiceName"].Value;
                }

                return serviceName;
            }
        }
    }
}
