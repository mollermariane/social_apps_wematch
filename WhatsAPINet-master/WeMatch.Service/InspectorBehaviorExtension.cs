﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    public class InspectorBehaviorExtension : BehaviorExtensionElement
    {

        public override Type BehaviorType
        {
            get
            {
                return typeof(InspectorBehavior);
            }
        }

        protected override object CreateBehavior()
        {
            return new InspectorBehavior();
        }

        [ConfigurationProperty("validateRequest", DefaultValue = false, IsRequired = false)]
        public bool ValidateRequest
        {
            get { return (bool)base["validateRequest"]; }
            set { base["validateRequest"] = value; }
        }

        [ConfigurationProperty("validateReply", DefaultValue = true, IsRequired = false)]
        public bool ValidateReply
        {
            get { return (bool)base["validateReply"]; }
            set { base["validateReply"] = value; }
        }

    }
}
