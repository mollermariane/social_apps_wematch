﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz.Impl.Calendar;
using Quartz;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    public static class Agendamentos
    {
        public static DailyCalendar ConstruirAgendamentoServico()
        {
            string inicio = "00-00";
            string fim = "23-59";
            return ConstruirAgendamentoServico(inicio, fim);
        }

        public static DailyCalendar ConstruirAgendamentoServico(string inicio, string fim)
        {
            string[] horaInicio = inicio.Split('-');
            string[] horaFim = fim.Split('-');

            DateTime dtInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(horaInicio[0]), Convert.ToInt32(horaInicio[1]), 0);
            DateTime dtFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(horaFim[0]), Convert.ToInt32(horaFim[1]), 0);

            DailyCalendar calendar = new DailyCalendar(dtInicio, dtFim);
            calendar.InvertTimeRange = true;
            return calendar;
        }
    }
}
