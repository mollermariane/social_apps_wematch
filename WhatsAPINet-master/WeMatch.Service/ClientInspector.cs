﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Threading.Tasks;

namespace WeMatch.Service
{
    public class ClientInspector : IClientMessageInspector
    {
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            var httpResponse = (System.ServiceModel.Channels.HttpResponseMessageProperty)reply.Properties["httpResponse"];

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK && (!httpResponse.StatusDescription.Contains("2000000 OK") && !httpResponse.StatusDescription.Contains("2000001") && !httpResponse.StatusDescription.Contains("OK")))
            {
                throw new FaultException(httpResponse.StatusDescription);
            }
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            return request;
        }
    }
}
