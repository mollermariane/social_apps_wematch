﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeMatch.Service.Tasks;

namespace WeMatch.Service
{
    public class TaskScheduler : ITaskScheduler
    {
        private IScheduler scheduler;

        public string Name
        {
            get { return GetType().Name; }
        }

        public void Run()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.GetScheduler();

#if DEBUG

            System.Diagnostics.Debugger.Launch();

#endif
            Inicializar();
            AdicionarJobs();
            scheduler.Start();
        }

        public void Stop()
        {
            scheduler.Shutdown();
        }

        private void AdicionarJobs()
        {
            AdicionaJobAgendado("GrupoDiario", "ControleHorario", "TaskCarona", typeof(TaskCarona));
        }

        private void AdicionaJobAgendado(string group, string calendarName, string jobName, Type Job)
        {
            scheduler.AddCalendar(calendarName, Agendamentos.ConstruirAgendamentoServico(), false, true);
            AdicionarJob(Job, new JobDataMap(), jobName, group, calendarName);
        }

        #region Métodos Auxiliares

        private void Inicializar()
        {
        }

        private void AdicionarJob(Type jobType, JobDataMap dataMap, string name, string group, string calendarName)
        {
            IJobDetail jobDetail = JobBuilder
                .Create()
                .OfType(jobType)
                .WithIdentity(new JobKey(name, group))
                .UsingJobData(dataMap)
                .Build();
            ITrigger trigger = TriggerBuilder
                .Create()
                .ForJob(jobDetail)
                .WithIdentity(name, group)
                .WithCronSchedule(ObterCron())
                .ModifiedByCalendar(calendarName)
                .StartNow()
                .Build();
            scheduler.ScheduleJob(jobDetail, trigger);
        }

        private static string ObterCron()
        {
            return ConfigurationManager.AppSettings["CronDiario"].ToString();
        }

        #endregion
    }
}
