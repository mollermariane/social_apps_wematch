﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Utils
{
    public static class TelefoneExtension
    {
        public static string RemoverCaracteresEspeciais(this String telefone)
        {
            string tel = string.Empty;
            if (string.IsNullOrEmpty(telefone))
                return null;
            tel = telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");

            return tel;
        }
    }
}
