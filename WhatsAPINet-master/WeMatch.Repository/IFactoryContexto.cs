﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Repository
{
    public interface IFactoryContexto
    {
        TContext ObterContextoAtual<TContext>() where TContext : DbContext;
        void LiberarContexto();
    }
}
