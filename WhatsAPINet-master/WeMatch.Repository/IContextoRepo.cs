﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeMatch.Models;

namespace WeMatch.Repository
{
    public interface IContextoRepo
    {
        IQueryable<QueroCarona> QueremCarona { get; }
        IQueryable<OferecoCarona> OferecemCarona { get; }
        IQueryable<CaronaSatisfeita> CaronaSatisfeita { get; }

        #region Métodos EE

        int SaveChanges();
        void Refresh<T>(T objectToRefresh);
        void Refresh<T>(IEnumerable<T> collectionToRefresh);
        T Attach<T>(T entity) where T : class;
        T Add<T>(T entity) where T : class;
        T Delete<T>(T entity) where T : class;

        #endregion
    }
}
