﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeMatch.Models;

namespace WeMatch.Repository
{
    public class ContextoRepo : DbContext, IContextoRepo
    {
        public ContextoRepo(string connectionString) : base(connectionString) { }

        public DbSet<QueroCarona> QueremCarona { get; set; }
        public DbSet<OferecoCarona> OferecemCarona { get; set; }
        public DbSet<CaronaSatisfeita> CaronaSatisfeita { get; set; }

        #region IContextoRepo Members

        IQueryable<QueroCarona> IContextoRepo.QueremCarona
        {
            get { return this.QueremCarona; }
        }

        IQueryable<OferecoCarona> IContextoRepo.OferecemCarona
        {
            get { return this.OferecemCarona; }
        }

        IQueryable<CaronaSatisfeita> IContextoRepo.CaronaSatisfeita
        {
            get { return this.CaronaSatisfeita; }
        }

        int IContextoRepo.SaveChanges()
        {
            return SaveChanges();
        }

        void IContextoRepo.Refresh<T>(T objectToRefresh)
        {
            var obj = (this as IObjectContextAdapter).ObjectContext;
            obj.Refresh(RefreshMode.StoreWins, objectToRefresh);
        }

        void IContextoRepo.Refresh<T>(IEnumerable<T> collectionToRefresh)
        {
            var obj = (this as IObjectContextAdapter).ObjectContext;
            obj.Refresh(RefreshMode.StoreWins, collectionToRefresh);
        }

        T IContextoRepo.Attach<T>(T entity)
        {
            var entry = Entry(entity);
            entry.State = EntityState.Modified;
            return entity;
        }

        T IContextoRepo.Add<T>(T entity)
        {
            return Set<T>().Add(entity);
        }

        T IContextoRepo.Delete<T>(T entity)
        {
            return Set<T>().Remove(entity);
        }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QueroCarona>().Property(l => l.Latitude).HasPrecision(13, 10);
            modelBuilder.Entity<QueroCarona>().Property(l => l.Longitude).HasPrecision(13, 10);
            modelBuilder.Entity<OferecoCarona>().Property(l => l.Longitude).HasPrecision(13, 10);
            modelBuilder.Entity<OferecoCarona>().Property(l => l.Longitude).HasPrecision(13, 10);
            //modelBuilder.Entity<QueroCarona>().
            //    HasMany(c => c.CaronasEncontradas).
            //    WithMany(p => p.CaronasEncontradas).
            //    Map(
            //        m =>
            //        {
            //            m.MapLeftKey("QueroCaronaId");
            //            m.MapRightKey("OferecoCaronaId");
            //            m.ToTable("CARONA_SATISFEITA");
            //        });

        }
    }
}
