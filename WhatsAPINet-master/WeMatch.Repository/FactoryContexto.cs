﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WeMatch.Repository
{
    public class FactoryContexto : IFactoryContexto
    {
        private static object syncRoot = new Object();
        private string connectionString;
        private volatile HttpContext httpContext;
        private const string ContextItemName = "Context_Per_Request";

        public FactoryContexto(HttpContext httpContext = null)
        {
            ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["WEMATCH_ConnectionString"];
            if (connection == null)
                throw new Exception("Conexão não encontrada ou inválida");

            this.connectionString = connection.ConnectionString;
            this.httpContext = httpContext;
        }

        /// <summary>
        /// Obtem um novo contexto
        /// </summary>
        /// <returns>Contexto</returns>
        //public static ContextoRepo ObterContexto()
        //{
        //    ConnectionStringSettings connection = ConfigurationManager.ConnectionStrings["SGC_ConnectionString"];
        //    if (connection == null)
        //        throw new ConnectionStringInvalidaOuInexistenteException();
        //    return CriarContexto(connection.ConnectionString);
        //}

        //private static ContextoRepo CriarContexto(string connectionString)
        //{
        //    ContextoRepo contexto = new ContextoRepo(connectionString);
        //    return contexto;
        //}

        public TContext ObterContextoAtual<TContext>() where TContext : System.Data.Entity.DbContext
        {
            TContext context = default(TContext);

            if (this.httpContext != null)
            {
                lock (syncRoot)
                    context = this.httpContext.Items[ContextItemName] as TContext;
            }

            if (context == null)
            {
                context = (TContext)Activator.CreateInstance(typeof(TContext), this.connectionString);
                
                if(this.httpContext != null)
                    this.httpContext.Items.Add(ContextItemName, context);
            }

            return context;
        }

        public void LiberarContexto()
        {
            DbContext context = default(DbContext);

            lock (syncRoot)
            {
                context = this.httpContext.Items[ContextItemName] as DbContext;
            }

            if (context != null)
                context.Dispose();
        }
    }
}
