﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Models
{
    [Table("OFERECO_CARONA")]
    public class OferecoCarona
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("OFCA_ID_QUERO_CARONA")]
        public int IdOferecoCarona { get; set; }

        [Column("OFCA_NM_USUARIO")]
        public string Nome { get; set; }

        [Column("OFCA_NR_TELEFONE")]
        public string Telefone { get; set; }

        [Column("OFCA_ID_MENSAGEM_DESTINO")]
        public string IdMensagemDestino { get; set; }

        [Column("OFCA_ID_MENSAGEM_LOCALIZACAO")]
        public string IdMensagemLocalizacao { get; set; }

        [Column("OFCA_TX_EMAIL")]
        public string Email { get; set; }

        [Column("OFCA_ID_CIDADE_DESTINO")]
        public int IdCidadeDestino { get; set; }

        //numeric(10,7)
        [Column("OFCA_NR_LATITUDE")]
        public decimal Latitude { get; set; }

        [Column("OFCA_NR_LONGITUDE")]
        public decimal Longitude { get; set; }

        [Column("OFCA_DT_SOLICITACAO")]
        public DateTime DataSolicitacao { get; set; }

        public virtual ICollection<CaronaSatisfeita> CaronasEncontradas { get; set; }
    }
}
