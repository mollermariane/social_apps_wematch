﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Models
{
    [Table("CARONA_SATISFEITA")]
    public class CaronaSatisfeita
    {
        [Column("OFCA_ID_QUERO_CARONA", Order = 0), Key, ForeignKey("OferecoCarona")]
        public int IdOferecoCarona { get; set; }

        [Column("QUCA_ID_QUERO_CARONA", Order = 1), Key, ForeignKey("QueroCarona")]
        public int IdQueroCarona { get; set; }

        public virtual OferecoCarona OferecoCarona { get; set; }
        public virtual QueroCarona QueroCarona { get; set; }
    }
}
