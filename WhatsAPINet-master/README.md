WeMatch - Using WhatsAppApi

Este projeto é composto por duas frentes: Um projeto Web e um projeto Windows Service. Além disso existe um banco de dados composto por 3 tabelas.

A estrutura do banco de dados está na pasta Database. Rodas os scripts na seguinte ordem:

1) script_carona1;
2) script_carona2;
3) script_carona3;

Para rodar o projeto Web basta realizar os seguintes passos:

1) Alterar chave “WEMATCH_ConnectionString” que tem por objetivo dizer onde está o banco de dados;
2) Publicar este projeto web em uma plataforma cloud ou no ias utilizando o framework 4.0

Para rodar o projeto de serviço basta realizar os seguintes passos:

1) Alterar chave “WEMATCH_ConnectionString” que tem por objetivo dizer onde está o banco de dados;
2) Alterar chave “sender” com o número do telefone que será utilizado para verificar e enviar as mensagens. Obs.: O número deve estar da seguinte maneira: ddi + ddd + telefone exemplo: 5519988888888;
3) Alterar a chave “password” com a senha gerada para o número do telefone escolhido. Para gerar essa senha dentro da pasta GetPasswordWhatsApp existe um arquivo chamado “WART-1.7.2.0.exe” rodar ele, passar o número de telefone seguindo a mesma regra do item 2 e ele vai mandar um código via SMS e logo em seguida coloque esse código e ele vai gerar um password;
4) Para rodar o serviço basta ir no projeto WhatsTest e rodar o exe que foi gerado após a compilação.