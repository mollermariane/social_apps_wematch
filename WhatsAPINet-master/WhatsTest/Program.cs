﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using WeMatch.Business;
using WeMatch.Models;
using WeMatch.Repository;
using WhatsAppApi;
using WeMatch.WhatsAppApi.Account;
using WeMatch.WhatsAppApi.Helper;
using WeMatch.WhatsAppApi.Register;
using WeMatch.WhatsAppApi.Response;
using System.Configuration;

namespace WeMatch.WhatsTest
{
    internal class Program
    {
        public static IContextoRepo contexto { get; set; }
        public static IFactoryContexto dbFactory { get; set; }
        public static CaronaBO CaronaBO { get; set; }
        public static WeMatch.WhatsAppApi.WhatsApp wa { get; set; }

        private static void Main(string[] args)
        {
            dbFactory = new FactoryContexto();
            contexto = dbFactory.ObterContextoAtual<ContextoRepo>();
            CaronaBO = new WeMatch.Business.CaronaBO(contexto);

            var tmpEncoding = Encoding.UTF8;
            System.Console.OutputEncoding = Encoding.Default;
            System.Console.InputEncoding = Encoding.Default;
            string nickname = ConfigurationManager.AppSettings["nickname"];
            string sender = ConfigurationManager.AppSettings["sender"];
            string password = ConfigurationManager.AppSettings["password"];
            string target = ConfigurationManager.AppSettings["target"];

            wa = new WeMatch.WhatsAppApi.WhatsApp(sender, password, nickname, true);

            //event bindings
            wa.OnLoginSuccess += wa_OnLoginSuccess;
            wa.OnLoginFailed += wa_OnLoginFailed;
            wa.OnGetMessage += wa_OnGetMessage;
            wa.OnGetMessageReceivedClient += wa_OnGetMessageReceivedClient;
            wa.OnGetMessageReceivedServer += wa_OnGetMessageReceivedServer;
            wa.OnNotificationPicture += wa_OnNotificationPicture;
            wa.OnGetPresence += wa_OnGetPresence;
            wa.OnGetGroupParticipants += wa_OnGetGroupParticipants;
            wa.OnGetLastSeen += wa_OnGetLastSeen;
            wa.OnGetTyping += wa_OnGetTyping;
            wa.OnGetPaused += wa_OnGetPaused;
            wa.OnGetMessageImage += wa_OnGetMessageImage;
            wa.OnGetMessageAudio += wa_OnGetMessageAudio;
            wa.OnGetMessageVideo += wa_OnGetMessageVideo;
            wa.OnGetMessageLocation += wa_OnGetMessageLocation;
            wa.OnGetMessageVcard += wa_OnGetMessageVcard;
            wa.OnGetPhoto += wa_OnGetPhoto;
            wa.OnGetPhotoPreview += wa_OnGetPhotoPreview;
            wa.OnGetGroups += wa_OnGetGroups;
            wa.OnGetSyncResult += wa_OnGetSyncResult;
            wa.OnGetStatus += wa_OnGetStatus;
            wa.OnGetPrivacySettings += wa_OnGetPrivacySettings;
            DebugAdapter.Instance.OnPrintDebug += Instance_OnPrintDebug;

            wa.Connect();

            string datFile = getDatFileName(sender);
            byte[] nextChallenge = null;
            if (File.Exists(datFile))
            {
                try
                {
                    string foo = File.ReadAllText(datFile);
                    nextChallenge = Convert.FromBase64String(foo);
                }
                catch (Exception) { };
            }

            wa.Login(nextChallenge);

            ProcessChat(wa, target);
            Console.ReadKey();
        }

        static void Instance_OnPrintDebug(object value)
        {
            Console.WriteLine(value);
        }

        static void wa_OnGetPrivacySettings(Dictionary<WeMatch.WhatsAppApi.ApiBase.VisibilityCategory, WeMatch.WhatsAppApi.ApiBase.VisibilitySetting> settings)
        {
            throw new NotImplementedException();
        }

        static void wa_OnGetStatus(string from, string type, string name, string status)
        {
            Console.WriteLine(String.Format("Got status from {0}: {1}", from, status));
        }

        static string getDatFileName(string pn)
        {
            string filename = string.Format("{0}.next.dat", pn);
            return Path.Combine(Directory.GetCurrentDirectory(), filename);
        }

        static void wa_OnGetSyncResult(int index, string sid, Dictionary<string, string> existingUsers, string[] failedNumbers)
        {
            Console.WriteLine("Sync result for {0}:", sid);
            foreach (KeyValuePair<string, string> item in existingUsers)
            {
                Console.WriteLine("Existing: {0} (username {1})", item.Key, item.Value);
            }
            foreach (string item in failedNumbers)
            {
                Console.WriteLine("Non-Existing: {0}", item);
            }
        }

        static void wa_OnGetGroups(WaGroupInfo[] groups)
        {
            Console.WriteLine("Got groups:");
            foreach (WaGroupInfo info in groups)
            {
                Console.WriteLine("\t{0} {1}", info.subject, info.id);
            }
        }

        static void wa_OnGetPhotoPreview(string from, string id, byte[] data)
        {
            Console.WriteLine("Got preview photo for {0}", from);
            File.WriteAllBytes(string.Format("preview_{0}.jpg", from), data);
        }

        static void wa_OnGetPhoto(string from, string id, byte[] data)
        {
            Console.WriteLine("Got full photo for {0}", from);
            File.WriteAllBytes(string.Format("{0}.jpg", from), data);
        }

        static void wa_OnGetMessageVcard(ProtocolTreeNode vcardNode, string from, string id, string name, byte[] data)
        {
            Console.WriteLine("Got vcard \"{0}\" from {1}", name, from);
            File.WriteAllBytes(string.Format("{0}.vcf", name), data);
        }

        static void wa_OnGetMessageLocation(ProtocolTreeNode locationNode, string from, string id, double lon, double lat, string url, string name, byte[] preview)
        {
            Console.WriteLine("Got location from {0} ({1}, {2})", from, lat, lon);
            if (!string.IsNullOrEmpty(name))
            {
                Console.WriteLine("\t{0}", name);
            }
            File.WriteAllBytes(string.Format("{0}{1}.jpg", lat, lon), preview);

            if (IsSolicitacaoCarona(from))
                ProcessarQueroCarona(from, name, id, null, null, lon, lat);
            else
                ProcessarOferecimentoCarona(from, name, id, null, null, lon, lat);
        }

        static void wa_OnGetMessageVideo(ProtocolTreeNode mediaNode, string from, string id, string fileName, int fileSize, string url, byte[] preview)
        {
            Console.WriteLine("Got video from {0}", from, fileName);
            OnGetMedia(fileName, url, preview);
        }

        static void OnGetMedia(string file, string url, byte[] data)
        {
            //save preview
            File.WriteAllBytes(string.Format("preview_{0}.jpg", file), data);
            //download
            using (WebClient wc = new WebClient())
            {
                wc.DownloadFileAsync(new Uri(url), file, null);
            }
        }

        static void wa_OnGetMessageAudio(ProtocolTreeNode mediaNode, string from, string id, string fileName, int fileSize, string url, byte[] preview)
        {
            Console.WriteLine("Got audio from {0}", from, fileName);
            OnGetMedia(fileName, url, preview);
        }

        static void wa_OnGetMessageImage(ProtocolTreeNode mediaNode, string from, string id, string fileName, int size, string url, byte[] preview)
        {
            Console.WriteLine("Got image from {0}", from, fileName);
            OnGetMedia(fileName, url, preview);
        }

        static void wa_OnGetPaused(string from)
        {
            Console.WriteLine("{0} stopped typing", from);
        }

        static void wa_OnGetTyping(string from)
        {
            Console.WriteLine("{0} is typing...", from);
        }

        static void wa_OnGetLastSeen(string from, DateTime lastSeen)
        {
            Console.WriteLine("{0} last seen on {1}", from, lastSeen.ToString());
        }

        static void wa_OnGetMessageReceivedServer(string from, string id)
        {
            Console.WriteLine("Message {0} to {1} received by server", id, from);
        }

        static void wa_OnGetMessageReceivedClient(string from, string id)
        {
            Console.WriteLine("Message {0} to {1} received by client", id, from);
        }

        static void wa_OnGetGroupParticipants(string gjid, string[] jids)
        {
            Console.WriteLine("Got participants from {0}:", gjid);
            foreach (string jid in jids)
            {
                Console.WriteLine("\t{0}", jid);
            }
        }

        static void wa_OnGetPresence(string from, string type)
        {
            Console.WriteLine("Presence from {0}: {1}", from, type);
        }

        static void wa_OnNotificationPicture(string type, string jid, string id)
        {
            //TODO
            //throw new NotImplementedException();
        }

        static void wa_OnGetMessage(ProtocolTreeNode node, string from, string id, string name, string message, bool receipt_sent)
        {
            Console.WriteLine("Message from {0} {1}: {2}", name, from, message);
            var mensagemQuebrada = message.Split(' ');
            string cidade = message.Replace(mensagemQuebrada.FirstOrDefault(), "").Trim();

            if (IsSolicitacaoCarona(from, message))
                ProcessarQueroCarona(from, name, null, id, cidade, null, null);
            else
                ProcessarOferecimentoCarona(from, name, null, id, cidade, null, null);
        }

        private static void wa_OnLoginFailed(string data)
        {
            Console.WriteLine("Login failed. Reason: {0}", data);
        }

        private static void wa_OnLoginSuccess(string phoneNumber, byte[] data)
        {
            Console.WriteLine("Login success. Next password:");
            string sdata = Convert.ToBase64String(data);
            Console.WriteLine(sdata);
            try
            {
                File.WriteAllText(getDatFileName(phoneNumber), sdata);
            }
            catch (Exception) { }
        }

        private static void ProcessChat(WeMatch.WhatsAppApi.WhatsApp wa, string dst)
        {
            var thRecv = new Thread(t =>
                                        {
                                            try
                                            {
                                                while (wa != null)
                                                {
                                                    wa.PollMessages();
                                                    Thread.Sleep(100);
                                                    continue;
                                                }

                                            }
                                            catch (ThreadAbortException)
                                            {
                                            }
                                        }) { IsBackground = true };
            thRecv.Start();

            WhatsUserManager usrMan = new WhatsUserManager();
            var tmpUser = usrMan.CreateUser(dst, "User");

            while (true)
            {
                string line = Console.ReadLine();
                if (line == null && line.Length == 0)
                    continue;

                string command = line.Trim();
                switch (command)
                {
                    case "/query":
                        //var dst = dst//trim(strstr($line, ' ', FALSE));
                        Console.WriteLine("[] Interactive conversation with {0}:", tmpUser);
                        break;
                    case "/accountinfo":
                        Console.WriteLine("[] Account Info: {0}", wa.GetAccountInfo().ToString());
                        break;
                    case "/lastseen":
                        Console.WriteLine("[] Request last seen {0}", tmpUser);
                        wa.SendQueryLastOnline(tmpUser.GetFullJid());
                        break;
                    case "/exit":
                        wa = null;
                        thRecv.Abort();
                        return;
                    case "/start":
                        wa.SendComposing(tmpUser.GetFullJid());
                        break;
                    case "/pause":
                        wa.SendPaused(tmpUser.GetFullJid());
                        break;
                    default:
                        Console.WriteLine("[] Send message to {0}: {1}", tmpUser, line);
                        wa.SendMessage(tmpUser.GetFullJid(), line);
                        break;
                }
            }
        }

        #region WeMatch

        private static void ProcessarOferecimentoCarona(string from, string name, string idLocalizacao = null, string idDestino = null, string message = null, double? lon = null, double? lat = null)
        {
            OferecoCarona queroCarona = ConverterModelEmOferecoCarona(from, name, message, lon, lat, idDestino, idLocalizacao);

            if (queroCarona != null)
                SalvarOferecoCarona(queroCarona, from, name);
        }

        private static bool IsSolicitacaoCarona(string from, string message = null)
        {
            if (string.IsNullOrEmpty(message))
            {
                string telefoneCorrente = from.Split('@').FirstOrDefault();

                QueroCarona pedido = contexto.QueremCarona.ToList().Where(l => l.Telefone.Equals(telefoneCorrente, StringComparison.CurrentCultureIgnoreCase)
                         && DateTime.Now.Subtract(l.DataSolicitacao).TotalMinutes <= 1
                         && ((l.Latitude == 0 && l.Longitude == 0 && l.IdCidadeDestino > 0) || (l.IdCidadeDestino == 0 && l.Latitude != 0 && l.Longitude != 0))).FirstOrDefault();

                return pedido != null;
            }
            else
            {
                List<string> listaOpcoes = new List<string> { "preciso", "quero", "solicito", "desejo", "requisito", "procuro" };
                foreach (var item in listaOpcoes)
                {
                    if (message.ToLower().Contains(item))
                    {
                        message = message.ToLower().Replace(item, "");
                        return true;
                    }
                }

                return false;
            }
        }

        private static void ProcessarQueroCarona(string from, string name, string idLocalizacao = null, string idDestino = null, string message = null, double? lon = null, double? lat = null)
        {
            QueroCarona queroCarona = ConverterModelEmQueroCarona(from, name, message, lon, lat, idDestino, idLocalizacao);

            if (queroCarona != null)
                SalvarQueroCarona(queroCarona, from, name);
        }

        private static void SalvarOferecoCarona(OferecoCarona oferecoCarona, string from, string name)
        {
            try
            {
                if (oferecoCarona.IdOferecoCarona > 0)
                    contexto.SaveChanges();
                else
                    CaronaBO.CriarOferecoCarona(oferecoCarona);

                if (!string.IsNullOrEmpty(oferecoCarona.IdMensagemDestino) && !string.IsNullOrEmpty(oferecoCarona.IdMensagemLocalizacao))
                {
                    List<QueroCarona> match = CaronaBO.VerificarMatchOferecoCarona(oferecoCarona);

                    if (match != null && match.Any())
                    {
                        foreach (var item in match)
                        {
                            string mensagem = string.Format("Encontramos uma oportunidade de oferecer carona para uma pessoa que está perto de você. Talvez ela entre em contato com você. Caso deseje, entre em contato com ela pelo telefone: {0} nome de contato: {1}", item.Telefone, item.Nome);
                            wa.SendMessage(from, mensagem);

                            string telefoneCorrente = from.Split('@').FirstOrDefault();
                            string telefoneOrigem = string.Format("{0}@s.whatsapp.net", item.Telefone);
                            string mensagemQuemOfereceu = string.Format("Encontramos uma oportunidade de carona de uma pessoa que está perto de você. Entre em contato com ela pelo telefone: {0} nome de contato: {1}", telefoneCorrente, oferecoCarona.Nome);
                            wa.SendMessage(telefoneOrigem, mensagemQuemOfereceu);  
                        }
                    }
                    else
                        wa.SendMessage(from, "Não encontramos um Match para você. Caso alguma solicitação seja feita dentro de 15 minutos você receberá uma mensagem deste celular via what's app.");
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static void SalvarQueroCarona(QueroCarona queroCarona, string from, string name)
        {
            try
            {
                if (queroCarona.IdQueroCarona > 0)
                    contexto.SaveChanges();
                else
                    CaronaBO.CriarQueroCarona(queroCarona);

                if (!string.IsNullOrEmpty(queroCarona.IdMensagemDestino) && !string.IsNullOrEmpty(queroCarona.IdMensagemLocalizacao))
                {
                    List<OferecoCarona> match = CaronaBO.VerificarMatchQueroCarona(queroCarona);

                    if (match != null && match.Any())
                    {
                        foreach (var item in match)
                        {
                            string mensagem = string.Format("Encontramos uma oportunidade de carona de uma pessoa que está perto de você. Entre em contato com ela pelo telefone: {0} nome de contato: {1}", item.Telefone, item.Nome);
                            wa.SendMessage(from, mensagem);

                            string telefoneCorrente = from.Split('@').FirstOrDefault();
                            string telefoneOrigem = string.Format("{0}@s.whatsapp.net", item.Telefone);
                            string mensagemQuemOfereceu = string.Format("Encontramos uma oportunidade de oferecer carona para uma pessoa que está perto de você. Talvez ela entre em contato com você. Caso deseje, entre em contato com ela pelo telefone: {0} nome de contato: {1}", telefoneCorrente, queroCarona.Nome);
                            wa.SendMessage(telefoneOrigem, mensagemQuemOfereceu);
                        }
                    }
                    else
                        wa.SendMessage(from, "Não encontramos um Match para você. Caso alguma solicitação seja feita dentro de 15 minutos você receberá uma mensagem deste celular via what's app.");
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static QueroCarona ConverterModelEmQueroCarona(string from, string name, string message = null, double? lon = null, double? lat = null, string idDestino = null, string idLocalizacao = null)
        {
            string telefoneCorrente = from.Split('@').FirstOrDefault();
            QueroCarona pedido = null;

            if (!string.IsNullOrEmpty(idDestino))
            {
                QueroCarona requisicaoJaFeita = contexto.QueremCarona.Where(l => l.IdMensagemDestino.Equals(idDestino, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (requisicaoJaFeita != null)
                    return null;
            }

            if (!string.IsNullOrEmpty(idLocalizacao))
            {
                QueroCarona requisicaoJaFeita = contexto.QueremCarona.Where(l => l.IdMensagemLocalizacao.Equals(idLocalizacao, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (requisicaoJaFeita != null)
                    return null;
            }

            pedido = contexto.QueremCarona.ToList().Where(l => l.Telefone.Equals(telefoneCorrente, StringComparison.CurrentCultureIgnoreCase)
                     && DateTime.Now.Subtract(l.DataSolicitacao).TotalMinutes <= 1
                     && ((l.Latitude == 0 && l.Longitude == 0 && l.IdCidadeDestino > 0) || (l.IdCidadeDestino == 0 && l.Latitude != 0 && l.Longitude != 0))).FirstOrDefault();

            if (pedido != null)
            {
                if (lon != null && lat != null && pedido.Longitude == 0 && pedido.Latitude == 0 && !string.IsNullOrEmpty(idLocalizacao))
                {
                    pedido.Longitude = Convert.ToDecimal(lon);
                    pedido.Latitude = Convert.ToDecimal(lat);
                    pedido.IdMensagemLocalizacao = idLocalizacao;
                }

                if (!string.IsNullOrEmpty(message) && pedido.IdCidadeDestino == 0 && !string.IsNullOrEmpty(idDestino))
                {
                    pedido.IdCidadeDestino = BuscarIdCidadeDestino(message);
                    pedido.IdMensagemDestino = idDestino;
                }

                return pedido;
            }
            else
            {
                QueroCarona obj = new QueroCarona()
                {
                    Nome = name,
                    Telefone = telefoneCorrente,
                    Email = "teste@teste.com",
                    IdCidadeDestino = !string.IsNullOrEmpty(message) ? BuscarIdCidadeDestino(message) : 0,
                    Latitude = lat != null ? Convert.ToDecimal(lat) : 0,
                    Longitude = lat != null ? Convert.ToDecimal(lon) : 0,
                    IdMensagemDestino = !string.IsNullOrEmpty(idDestino) ? idDestino : null,
                    IdMensagemLocalizacao = !string.IsNullOrEmpty(idLocalizacao) ? idLocalizacao : null
                };

                return obj;
            }
        }

        private static OferecoCarona ConverterModelEmOferecoCarona(string from, string name, string message = null, double? lon = null, double? lat = null, string idDestino = null, string idLocalizacao = null)
        {
            string telefoneCorrente = from.Split('@').FirstOrDefault();
            OferecoCarona oferta = null;

            if (!string.IsNullOrEmpty(idDestino))
            {
                OferecoCarona requisicaoJaFeita = contexto.OferecemCarona.Where(l => l.IdMensagemDestino.Equals(idDestino, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (requisicaoJaFeita != null)
                    return null;
            }

            if (!string.IsNullOrEmpty(idLocalizacao))
            {
                OferecoCarona requisicaoJaFeita = contexto.OferecemCarona.Where(l => l.IdMensagemLocalizacao.Equals(idLocalizacao, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                if (requisicaoJaFeita != null)
                    return null;
            }

            oferta = contexto.OferecemCarona.ToList().Where(l => l.Telefone.Equals(telefoneCorrente, StringComparison.CurrentCultureIgnoreCase)
                     && DateTime.Now.Subtract(l.DataSolicitacao).TotalMinutes <= 1
                     && ((l.Latitude == 0 && l.Longitude == 0 && l.IdCidadeDestino > 0) || (l.IdCidadeDestino == 0 && l.Latitude != 0 && l.Longitude != 0))).FirstOrDefault();

            if (oferta != null)
            {
                if (lon != null && lat != null && oferta.Longitude == 0 && oferta.Latitude == 0 && !string.IsNullOrEmpty(idLocalizacao))
                {
                    oferta.Longitude = Convert.ToDecimal(lon);
                    oferta.Latitude = Convert.ToDecimal(lat);
                    oferta.IdMensagemLocalizacao = idLocalizacao;
                }

                if (!string.IsNullOrEmpty(message) && oferta.IdCidadeDestino == 0 && !string.IsNullOrEmpty(idDestino))
                {
                    oferta.IdCidadeDestino = BuscarIdCidadeDestino(message);
                    oferta.IdMensagemDestino = idDestino;
                }

                return oferta;
            }
            else
            {
                OferecoCarona obj = new OferecoCarona()
                {
                    Nome = name,
                    Telefone = telefoneCorrente,
                    Email = "teste@teste.com",
                    IdCidadeDestino = !string.IsNullOrEmpty(message) ? BuscarIdCidadeDestino(message) : 0,
                    Latitude = lat != null ? Convert.ToDecimal(lat) : 0,
                    Longitude = lat != null ? Convert.ToDecimal(lon) : 0,
                    IdMensagemDestino = !string.IsNullOrEmpty(idDestino) ? idDestino : null,
                    IdMensagemLocalizacao = !string.IsNullOrEmpty(idLocalizacao) ? idLocalizacao : null
                };

                return obj;
            }
        }

        private static int BuscarIdCidadeDestino(string message)
        {
            switch (message.ToUpper().Trim())
            {
                case ("AMERICANA"):
                    return 1;
                case ("CAMPINAS"):
                    return 2;
                case ("INDAIATUBA"):
                    return 3;
                case ("MONTE MOR"):
                    return 4;
                case ("SUMARÉ"):
                    return 5;
                case ("NOVA ODESSA"):
                    return 6;
                default:
                    return 0;
            }
        }

        #endregion
    }
}
