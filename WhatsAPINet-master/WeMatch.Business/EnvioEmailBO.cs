﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.Business
{
    public static class EnvioEmailBO
    {
        private static MailMessage PreencherListaEnderecoEmails(string enderecosEmails, MailMessage mailMessage)
        {
            foreach (string enderecoEmail in enderecosEmails.Split(','))
            {
                mailMessage.To.Add(new MailAddress(enderecoEmail));
            }

            return mailMessage;
        }

        private static void Enviar(string emailMensagem, string emailsPara, string emailAssunto, bool emailAutenticado, bool isHtml)
        {
            try
            {
                SmtpClient sender;

                if (emailAutenticado)
                {
                    sender = new SmtpClient("smtp.gmail.com", Convert.ToInt32("587"));
                    sender.Credentials = new System.Net.NetworkCredential("wematchservice@gmail.com", "12345senha");
                    sender.EnableSsl = true;
                }
                else
                {
                    sender = new SmtpClient("smtp.gmail.com");
                }

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("wematchservice@gmail.com");
                mailMessage = PreencherListaEnderecoEmails(emailsPara, mailMessage);
                mailMessage.Body = emailMensagem;
                mailMessage.IsBodyHtml = isHtml;
                mailMessage.Subject = emailAssunto;

                sender.Send(mailMessage);

            }
            catch (Exception exEmail)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("Ocorreu um erro ao enviar o email de notificação.");
                sb.AppendLine(exEmail.Message);
                if (exEmail.InnerException != null)
                {
                    sb.AppendLine(" - " + exEmail.InnerException.Message + " - ");
                }
                sb.AppendLine(exEmail.StackTrace);
            }
        }

        public static string BuscaCorpoEmail(string nomeTemplate)
        {
            var template = BuscaCorpoEmailStringBuilder(nomeTemplate);

            return template.ToString();
        }

        public static StringBuilder BuscaCorpoEmailStringBuilder(string nomeTemplate)
        {
            var template = new StringBuilder();
            string pathTemplate = null;

            string urlTemplate = ConfigurationManager.AppSettings["UrlTemplateEmail"].ToString();

            if (string.IsNullOrEmpty(urlTemplate))
                pathTemplate = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            else
                pathTemplate = urlTemplate;

            if (string.IsNullOrEmpty(pathTemplate) || string.IsNullOrEmpty(nomeTemplate))
                return null;

            pathTemplate = MontaUrlArquivo(pathTemplate, nomeTemplate);

            string linhaTemplate = string.Empty;
            StreamReader readerTemplate = null;

            readerTemplate = new StreamReader(pathTemplate, Encoding.UTF8);

            while ((linhaTemplate = readerTemplate.ReadLine()) != null)
                template.AppendLine(linhaTemplate);

            return template;
        }

        private static string MontaUrlArquivo(string pathTemplate, string nomeTemplate)
        {
            if (string.IsNullOrEmpty(pathTemplate) || string.IsNullOrEmpty(nomeTemplate))
                return string.Empty;

            pathTemplate = pathTemplate + @"\EmailTemplate\" + nomeTemplate + ".html";

            return pathTemplate;
        }

        private static string MontarCorpoEmail(string nomeCliente, string mensagem)
        {
            string corpoEmail = BuscaCorpoEmail("Carona");
            string corpoEmailPreenchido = string.Empty;
            if (!string.IsNullOrEmpty(corpoEmail))
            {
                corpoEmailPreenchido = string.Format(corpoEmail,
                    nomeCliente,
                    mensagem);
            }
            return corpoEmailPreenchido;
        }

        public static void EnviarEmailMatchQueroCaronaEncontrado(Models.OferecoCarona oferecoCarona, Models.QueroCarona queroCarona)
        {
            string mensagem = string.Format("Encontramos uma oportunidade de carona de uma pessoa que está perto de você. Entre em contato com ela pelo telefone: {0} nome de contato: {1}", oferecoCarona.Telefone, oferecoCarona.Nome);

            //string corpoEmailPreenchido = MontarCorpoEmail(queroCarona.Nome, mensagem);
            string corpoEmailPreenchido = string.Format("Olá {0} <br/><br/>{1}", queroCarona.Nome, mensagem);

            Enviar(corpoEmailPreenchido, queroCarona.Email, "Carona Encontrada", true, true);
        }

        public static void EnviarEmailMatchOferecoCaronaEncontrado(Models.OferecoCarona oferecoCarona, Models.QueroCarona queroCarona)
        {
            string mensagem = string.Format("Encontramos uma oportunidade de oferecer carona para uma pessoa que está perto de você. Talvez ela entre em contato com você. Caso deseje, entre em contato com ela pelo telefone: {0} nome de contato: {1}", queroCarona.Telefone, queroCarona.Nome);

            //string corpoEmailPreenchido = MontarCorpoEmail(oferecoCarona.Nome, mensagem);
            string corpoEmailPreenchido = string.Format("Olá {0} <br/><br/>{1}", oferecoCarona.Nome, mensagem);

            Enviar(corpoEmailPreenchido, oferecoCarona.Email, "Carona Encontrada", true, true);
        }

        public static void EnviarEmailMatchOferecoCaronaNaoEncontrado(Models.OferecoCarona oferecoCarona)
        {
            string mensagem = "Não encontramos nenhuma oportunidade de oferecer carona na região onde você está e neste período de tempo. Caso exista alguma solicitação de carona por pessoas que estejam perto de você nos próximos 15 minutos enviamos um e-mail.";

            //string corpoEmailPreenchido = MontarCorpoEmail(oferecoCarona.Nome, mensagem);
            string corpoEmailPreenchido = string.Format("Olá {0} <br/><br/>{1}", oferecoCarona.Nome, mensagem);

            Enviar(corpoEmailPreenchido, oferecoCarona.Email, "Carona Não Encontrada", true, true);
        }

        public static void EnviarEmailMatchQueroCaronaNaoEncontrado(Models.QueroCarona queroCarona)
        {
            string mensagem = "Não encontramos nenhuma oportunidade de carona na região onde você está e neste período de tempo. Caso exista algum oferecimento de carona por pessoas que estejam perto de você nos próximos 15 minutos enviamos um e-mail.";

            //string corpoEmailPreenchido = MontarCorpoEmail(queroCarona.Nome, mensagem);
            string corpoEmailPreenchido = string.Format("Olá {0} <br/><br/>{1}", queroCarona.Nome, mensagem);

            Enviar(corpoEmailPreenchido, queroCarona.Email, "Carona Não Encontrada", true, true);
        }
    }
}
