﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeMatch.Models;
using WeMatch.Repository;

namespace WeMatch.Business
{
    public class CaronaBO
    {
        public IContextoRepo Contexto { get; set; }
        public GeoBO GeoBO { get; set; }

        public CaronaBO(IContextoRepo contexto)
        {
            Contexto = contexto;
            GeoBO = new GeoBO();
        }

        public List<OferecoCarona> ObterTodosOsOferecimentos()
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("pt-BR");
            DateTime dataAtual = DateTime.Parse(DateTime.Now.ToString(cultureinfo), cultureinfo);

            return Contexto.OferecemCarona != null && Contexto.OferecemCarona.Any() ?
                //Contexto.OferecemCarona.ToList().Where(l => dataAtual.Subtract(DateTime.Parse(l.DataSolicitacao.ToString(cultureinfo), cultureinfo)).TotalMinutes <= 15 && l.IdCidadeDestino > 0 && l.Latitude != 0 && l.Longitude != 0).OrderByDescending(l => l.DataSolicitacao).ToList()
                Contexto.OferecemCarona.ToList().Where(l => l.IdCidadeDestino > 0 && l.Latitude != 0 && l.Longitude != 0).OrderByDescending(l => l.DataSolicitacao).ToList()
                : null;
        }

        public List<QueroCarona> ObterTodasAsSolicitacoes()
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("pt-BR");
            DateTime dataAtual = DateTime.Parse(DateTime.Now.ToString(cultureinfo), cultureinfo);

            return Contexto.QueremCarona != null && Contexto.QueremCarona.Any() ?
                //Contexto.QueremCarona.ToList().Where(l => dataAtual.Subtract(DateTime.Parse(l.DataSolicitacao.ToString(cultureinfo), cultureinfo)).TotalMinutes <= 15 && l.IdCidadeDestino > 0 && l.Latitude != 0 && l.Longitude != 0).OrderByDescending(l => l.DataSolicitacao).ToList()
                Contexto.QueremCarona.ToList().Where(l => l.IdCidadeDestino > 0 && l.Latitude != 0 && l.Longitude != 0).OrderByDescending(l => l.DataSolicitacao).ToList()
                : null;
        }

        public List<CaronaSatisfeita> ObterMatchs()
        {
            return Contexto.CaronaSatisfeita.ToList();
        }

        public QueroCarona CriarQueroCarona(QueroCarona queroCarona)
        {
            try
            {
                queroCarona.DataSolicitacao = DateTime.Now;
                queroCarona = Contexto.Add<QueroCarona>(queroCarona);
                Contexto.SaveChanges();
                return queroCarona;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public OferecoCarona CriarOferecoCarona(OferecoCarona oferecoCarona)
        {
            try
            {
                oferecoCarona.DataSolicitacao = DateTime.Now;
                oferecoCarona = Contexto.Add<OferecoCarona>(oferecoCarona);
                Contexto.SaveChanges();
                return oferecoCarona;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public CaronaSatisfeita CriarCaronaSatisfeita(CaronaSatisfeita caronaSatisfeita)
        {
            try
            {
                caronaSatisfeita = Contexto.Add<CaronaSatisfeita>(caronaSatisfeita);
                Contexto.SaveChanges();
                return caronaSatisfeita;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public List<OferecoCarona> VerificarMatchQueroCarona(QueroCarona queroCarona)
        {
            try
            {
                List<OferecoCarona> encontrouAlgumMatch = null;
                var listaCaronas = Contexto.OferecemCarona != null && Contexto.OferecemCarona.Any() ?
                                Contexto.OferecemCarona.ToList()
                                .Where(l => l.IdCidadeDestino == queroCarona.IdCidadeDestino &&
                                    ((l.CaronasEncontradas == null || !l.CaronasEncontradas.Any()) ||
                                        (l.CaronasEncontradas.Count(c => c.IdOferecoCarona == l.IdOferecoCarona && c.IdQueroCarona == queroCarona.IdQueroCarona) == 0))
                                        //(GeoBO.distance(Convert.ToDouble(queroCarona.Latitude), Convert.ToDouble(queroCarona.Longitude), Convert.ToDouble(l.Latitude), Convert.ToDouble(l.Longitude)) <= 200))
                                        && queroCarona.DataSolicitacao.Subtract(l.DataSolicitacao).TotalMinutes <= 15)
                                .ToList()
                                : null;

                if (listaCaronas != null && listaCaronas.Any())
                {
                    encontrouAlgumMatch = new List<OferecoCarona>();
                    foreach (var item in listaCaronas)
                    {
                        encontrouAlgumMatch.Add(item);
                        //EnvioEmailBO.EnviarEmailMatchQueroCaronaEncontrado(item, queroCarona);
                        //EnvioEmailBO.EnviarEmailMatchOferecoCaronaEncontrado(item, queroCarona);
                        CaronaSatisfeita caronaSatisfeita = new CaronaSatisfeita { IdQueroCarona = queroCarona.IdQueroCarona, IdOferecoCarona = item.IdOferecoCarona };
                        CriarCaronaSatisfeita(caronaSatisfeita);
                    }
                }
                //else
                //    EnvioEmailBO.EnviarEmailMatchQueroCaronaNaoEncontrado(queroCarona);

                return encontrouAlgumMatch;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<QueroCarona> VerificarMatchOferecoCarona(OferecoCarona oferecoCarona)
        {
            try
            {
                List<QueroCarona> encontrouAlgumMatch = null;
                var listaCaronasRequisitadas = Contexto.QueremCarona != null && Contexto.QueremCarona.Any() ?
                                                Contexto.QueremCarona.ToList()
                                                .Where(l => l.IdCidadeDestino == oferecoCarona.IdCidadeDestino &&
                                                    ((l.CaronasEncontradas == null || !l.CaronasEncontradas.Any()) ||
                                                        l.CaronasEncontradas.Count(c => c.IdOferecoCarona == oferecoCarona.IdOferecoCarona && c.IdQueroCarona == l.IdQueroCarona) == 0) &&
                                                        //(GeoBO.distance(Convert.ToDouble(oferecoCarona.Latitude), Convert.ToDouble(oferecoCarona.Longitude), Convert.ToDouble(l.Latitude), Convert.ToDouble(l.Longitude)) <= 200) &&
                                                        oferecoCarona.DataSolicitacao.Subtract(l.DataSolicitacao).TotalMinutes <= 15)
                                                .ToList()
                                                : null;

                if (listaCaronasRequisitadas != null && listaCaronasRequisitadas.Any())
                {
                    encontrouAlgumMatch = new List<QueroCarona>();
                    foreach (var item in listaCaronasRequisitadas)
                    {
                        encontrouAlgumMatch.Add(item);
                        //EnvioEmailBO.EnviarEmailMatchQueroCaronaEncontrado(oferecoCarona, item);
                        //EnvioEmailBO.EnviarEmailMatchOferecoCaronaEncontrado(oferecoCarona, item);
                        CaronaSatisfeita caronaSatisfeita = new CaronaSatisfeita { IdQueroCarona = item.IdQueroCarona, IdOferecoCarona = oferecoCarona.IdOferecoCarona };
                        CriarCaronaSatisfeita(caronaSatisfeita);
                    }
                }
                //else
                //    EnvioEmailBO.EnviarEmailMatchOferecoCaronaNaoEncontrado(oferecoCarona);

                return encontrouAlgumMatch;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
