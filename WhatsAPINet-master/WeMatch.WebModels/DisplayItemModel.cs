﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.WebModels
{
    public class DisplayItemModel
    {
        public string MensagemPost { get; set; }
        public bool IsOferecimentoCarona { get; set; }
        public DateTime Data { get; set; }
    }
}
