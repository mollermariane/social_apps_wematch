﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WeMatch.WebModels
{
    public class CaronaModel
    {
        [Display(Name="*Nome")]
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Nome { get; set; }

        [Display(Name = "*Telefone")]
        [Required(ErrorMessage = "Campo obrigatório")]
        public string Telefone { get; set; }

        [Display(Name = "*E-mail")]
        [Required(ErrorMessage = "Campo obrigatório")]
        [RegularExpression(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*", ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public SelectList Cidades { get; set; }

        public SelectList TiposUsuario { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public int? IdCidade { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public int? IdTipoUSuario { get; set; }
    }
}
