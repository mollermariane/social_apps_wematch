﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeMatch.Models;

namespace WeMatch.WebModels
{
    public class DisplayModel
    {
        public List<DisplayItemModel> ListaOferecimentos { get; set; }
        public List<DisplayItemModel> ListaSolicitacoes { get; set; }
        public List<DisplayItemModel> ListaCaronaSatisfeita { get; set; }
    }
}
