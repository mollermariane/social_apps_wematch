﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeMatch.WebModels.Enum
{
    public enum EnumCidade
    {
        Americana = 1,
        Campinas = 2,
        Indaiatuba = 3,
        MonteMor = 4,
        Sumare = 5,
        NovaOdesa = 6
    }
}
